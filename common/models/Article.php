<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property integer $id
 * @property integer $cid
 * @property string $title
 * @property integer $visitNums
 * @property string $keywords
 * @property string $description
 * @property integer $status
 * @property string $summary
 * @property string $thumbnail
 * @property string $content
 * @property integer $isDel
 * @property integer $uTime
 * @property integer $cUid
 * @property integer $cTime
 * @property integer $uUid
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'visitNums', 'status', 'isDel', 'uTime', 'cUid', 'cTime', 'uUid'], 'integer'],
            [['visitNums', 'isDel'], 'required'],
            [['description', 'content'], 'string'],
            [['title'], 'string', 'max' => 200],
            [['keywords'], 'string', 'max' => 50],
            [['summary'], 'string', 'max' => 255],
            [['thumbnail'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('article', 'ID'),
            'cid' => Yii::t('article', 'Cid'),
            'title' => Yii::t('article', 'Title'),
            'visitNums' => Yii::t('article', 'Visit Nums'),
            'keywords' => Yii::t('article', 'Keywords'),
            'description' => Yii::t('article', 'Description'),
            'status' => Yii::t('article', 'Status'),
            'summary' => Yii::t('article', 'Summary'),
            'thumbnail' => Yii::t('article', 'Thumbnail'),
            'content' => Yii::t('article', 'Content'),
            'isDel' => Yii::t('article', 'Is Del'),
            'uTime' => Yii::t('article', 'U Time'),
            'cUid' => Yii::t('article', 'C Uid'),
            'cTime' => Yii::t('article', 'C Time'),
            'uUid' => Yii::t('article', 'U Uid'),
        ];
    }
}
