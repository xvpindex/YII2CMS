<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%channel}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property integer $created_at
 * @property string $created_by
 * @property integer $updated_at
 * @property string $updated_by
 */
class Channel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%channel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['created_by', 'updated_by'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('chanel', 'ID'),
            'name' => Yii::t('chanel', 'Name'),
            'content' => Yii::t('chanel', 'Content'),
            'created_at' => Yii::t('chanel', 'Created At'),
            'created_by' => Yii::t('chanel', 'Created By'),
            'updated_at' => Yii::t('chanel', 'Updated At'),
            'updated_by' => Yii::t('chanel', 'Updated By'),
        ];
    }
}
