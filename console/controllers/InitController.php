<?php

namespace console\controllers;

use common\models\User;
class InitController extends \yii\console\Controller
{
	
	public function actionUser()
	{
		echo "create init user ..\n";
		$username = $this->prompt('Input Username:');
		$email = $this->prompt('Input Email:');
		$password = $this->prompt('Input Password:');

		$model = new User();
		$model->username = $username;
		$model->email = $email;
		$model->password = $password;
		
		if(!$model->save())
		{
			foreach($model->getErrors as $errors){
				foreach($errors as $e){
					echo "$e\n";
				}
			}
			return 1;
		}
		return 0;		
	}

}
