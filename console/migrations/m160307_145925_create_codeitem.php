<?php

use yii\db\Migration;

class m160307_145925_create_codeitem extends Migration
{
    public function up()
    {
        $this->createTable('{{%codeitem}}', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(5)->notNull(),
            'text' => $this->string(50)->notNull(),
            'iconCLs' => $this->string(10)->notNull(),
            'state' => $this->string(6)->notNull(),
            'checked' => $this->string(50),
            'children' => $this->string(50)
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%codeitem}}');
    }
}
