<?php

use yii\db\Migration;

class m160305_003213_create_channel extends Migration
{
    public function up()
    {
        $this->createTable('{{%channel}}', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer()->notNull(),
            'name' => $this->string(32)->notNull(),
            'content' => $this->text(),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(), 
            'created_by' => $this->string(10)->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'updated_by' => $this->string(10)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%channel}}');
    }
}
