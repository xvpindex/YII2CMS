<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChannelController implements the CRUD actions for Channel model.
 */
class ShellController extends Controller
{

    /**
     * Lists all Channel models.
     * @return mixed
     */
    public function actionIndex()
    {
            //exec("sh /home/test.sh");
            $result = shell_exec("ls -l /home");
            echo "<pre>";
            print_r($result);
            echo "</pre>";
    }

    public function actionTest1()
    {
            set_time_limit(0);
            $output = array();
            $ret = 0;
            exec("/usr/bin/sudo /bin/sh /home/test.sh aabbcc", $output, $ret);
            echo "Result:{$ret}";
            print_r($output);
    }

    public function actionCreate_vhost()
    {
            set_time_limit(0);
            $output = array();
            $ret = 0;
            exec("/usr/bin/sudo /bin/sh /home/wwwroot/ewsdShell/shell/vhost_create.sh www.aabb.com", $output, $ret);
            echo "Result:{$ret}";
            print_r($output);
    }
 
    public function actionTest2()
    {
            set_time_limit(0);
            //$output = array();
            $ret = 0;
            exec("sudo mkdir /home/aaa", $output, $ret);
            echo "Result:{$ret}";
            print_r($output);
    }

}
