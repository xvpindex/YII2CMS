<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Channel */

$this->title = Yii::t('channel', 'Update {modelClass}: ', [
    'modelClass' => 'Channel',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('channel', 'Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('channel', 'Update');
?>
<div class="channel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
